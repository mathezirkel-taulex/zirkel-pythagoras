% Hier den Pfad zu zirkel.cls angeben
\documentclass{../tex-bilder/zirkel}

\graphicspath{{../tex-bilder/}}

\usepackage{mdframed}
\usepackage{array}      % for spacing in tabular

\usepackage{wrapfig,lipsum}     % image to the side with text wrapped around

\renewcommand{\schuljahr}{2023/24}
\renewcommand{\klasse}{7 \& 8}
\renewcommand{\titel}{Satz des Pythagoras, Teil 2/2}
\renewcommand{\untertitel}{Präsenzzirkel vom 23. Januar 2024}
\renewcommand{\name}{Alexander Mai}

% Vordefinierte Umgebungen
%     * aufgabe
%     * hinweis
%     * loesung
%     * satz
%     * beispiel

\begin{document}
    \makeheader

    Im Kennenlernzirkel haben wir uns bereits kurz mit dem \emph{Satz des Pythagoras} befasst.

    \begin{satz}
        Für jedes rechtwinklige Dreieck, dessen Seitenlängen wir so mit $a, b$ und $c$ bezeichnen, dass $c$ die längste Seite ist, gilt:

        $$ a^2 + b^2 = c^2$$
    \end{satz}

    \begin{wrapfigure}{r}{5.5cm}
        \vspace*{-2em}
        \includegraphics[height=15.5em]{pythagoras/wiki-pythagoras.pdf}
    \end{wrapfigure}

    Bildlich interpretiert bedeutet das, dass die oberen beiden Quadrate im nebenstehenden Bild zusammen den gleichen Flächeninhalt haben, wie das untere Quadrat alleine.

    % \begin{figure}[h!]
    %     \centering
    %     \includegraphics[height=10em]{pythagoras/wiki-pythagoras.pdf}
    %     % \caption{Description}
    % \end{figure}
    % \vspace*{1em}

    \begin{hinweis}
        Die längste Seite eines rechtwinkligen Dreiecks ist immer gegenüber vom rechten Winkel.
    \end{hinweis}

    Heute wollen wir uns mit praktischen Anwendungen und Beweisen des Satzes befassen. Für diesen Satz gibt es besonders viele sogenannte \emph{Beweise ohne Worte}. Das sind solche Beweise, die sich größtenteils auch ohne Vorwissen durch aussagekräftige Bilder und deine Vorstellungskraft vor deinen Augen zusammensetzen. Mit ein bisschen Übung kannst du die Beweise verstehen.

    \section{\glqq Hoch 2\grqq\ und \glqq Wurzel von\grqq}

    \begin{definition}[$x^2$]
        Für eine beliebige Zahl $x$ schreiben wir $x \cdot x$ auch als $x^2 .$

        Gesprochen wird es oft als \glqq \emph{$x$ hoch zwei}\grqq\ oder \glqq \emph{$x$ quadriert}\grqq\ oder \glqq \emph{$x$ im Quadrat}\grqq . Für die Anwendung von \glqq \emph{hoch zwei}\grqq\ wird im Sprachgebrauch häufig das Verb \glqq \emph{quadrieren}\grqq\ verwendet.
    \end{definition}

    \begin{aufgabe}
        Berechne das Ergebnis von
        \begin{center}
            \begin{tabular}{ m{\linewidth / 4} m{\linewidth / 3} m{\linewidth / 3} }
                a) $3^2 =$      & d) $1{,}5^2 =$    & g) $1000^2 =$ \\
                \\
                b) $10^2 =$     & e) $1^2 =$        & h) $5^3 =$ \\
                \\
                c) $(-7)^2 =$      & f) $-15^2 =$       & i) $2^4 =$ \\
            \end{tabular}
        \end{center}
        % \begin{enumerate}[a)]
        %     \item $3^2$
        %     \item $10^2$
        %     \item $7^2$
        %     \item $1{,}5^2$
        %     \item $1^2$
        %     \item $15^2$
        %     \item $1000^2$
        %     \item $5^3$
        %     \item $2^4$
        % \end{enumerate}
    \end{aufgabe}

    \vspace*{-3em}

    \begin{hinweis}
        Statt $2$ in $x^2$ kann auch jede andere beliebige Zahl eingesetzt werden. Bei positiven ganzen Zahlen multiplizieren wir die Zahl $x$ einfach häufiger mit sich selbst, etwa $x^5 = x \cdot x \cdot x \cdot x \cdot x.$ Bei negativen ganzen Zahlen müssen wir den Kehrbruch nehmen. Und für alle anderen Zahlen können wir uns bereits auf den Mathematikunterricht in den letzten Schuljahren freuen.
    \end{hinweis}



    \begin{definition}[$\sqrt{x}$]
        Für eine beliebige positive Zahl $x$ schreiben wir $\sqrt{x}$ für die \glqq \emph{Quadratwurzel von $x$}\grqq\ oder häufig einfach \glqq \emph{Wurzel von $x$}\grqq .

        Die Wurzel ist gewissermaßen das Gegenteil von \glqq \emph{$x^2$}\grqq , denn als Ergebnis kommt bei der Wurzel genau so eine Zahl heraus, die quadriert wieder $x$ ergibt. Also gilt:
        $$ (\sqrt{x})^2 = x .$$
    \end{definition}

    \begin{aufgabe}
        Berechne das Ergebnis von
        \begin{center}
            \begin{tabular}{ m{\linewidth / 3} m{\linewidth / 2} m{\linewidth / 12} }
                a) $\sqrt{4} =$      & e) $\sqrt{25} =$ & \\
                \\
                b) $\sqrt{36} =$     & f) $\sqrt{9} + \sqrt{16} =$ \\
                \\
                c) $\sqrt{100} =$      & g) $\sqrt{5} \approx$ \\
                \\
                d) $\sqrt{1} =$      & h) $\sqrt{20} \approx$ \\
            \end{tabular}
        \end{center}
        % \begin{enumerate}[a)]
        %     \item $\sqrt{4}$
        %     \item $\sqrt{36}$
        %     \item $\sqrt{100}$
        %     \item $\sqrt{1}$
        %     \item $\sqrt{25}$
        %     \item $\sqrt{5}$
        %     \item $\sqrt{20}$
        % \end{enumerate}
    \end{aufgabe}


    \section{Mathe und Handwerk}
    \vspace*{-1em}

    \begin{figure}[h!]
        \centering
        \includegraphics[height=10em]{pythagoras/picture-pythagoras-building.pdf}
        % \caption{Description}
    \end{figure}

    Du baust ein Zimmer und musst noch einen Balken besorgen gehen, dessen Länge genau richtig ist für die letzten beiden Befestigungspunkte. Dabei stellst du zu deinem Erschrecken fest, dass dein Maßband zu kurz ist und du damit den Abstand von Befestigung zu Befestigung nicht messen kannst. Es reicht aber gerade so, dass du jeweils den Abstand der Befestigung von der Kante messen kannst.


    \begin{aufgabe}
        \label{ex:pyth}
        Wie lang müsste der zu besorgende Balken sein, wenn $a$ und $b$ jeweils folgende Längen haben?
        \begin{enumerate}[i)]
            \item $a = 4, \quad b = 3$ % -> c = 5
            \item $a = 8, \quad b = 15$ % -> c = 17
            \item $a = 1, \quad b = 1$ % -> c = sqrt(2)
        \end{enumerate}
    \end{aufgabe}


    \newpage
    \vspace*{-7em}

    \section{Für diese Beweise fehlen mir die Worte}

    % Achtung: die Bilder verleiten dazu, alles einfach so zu akzeptieren und wesentliche Argumente nicht zu führen. Im Folgenden jeweils wichtige Argumente, auf die man hinweisen sollte, damit die Kinder sie auch selbst durchdenken.

    \subsection{Der klassische Beweis -- Schiebung}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=35em]{pythagoras/proof-pyth-classic.pdf}
        % Animation: https://www.geogebra.org/m/jFFERBdd#material/MJWHp9en
        % Argumente: Was ist der Flächeninhalt von der blauen Fläche im mittleren Bild? Welche Seitenlängen hat das große Quadrat und warum ist es überhaupt ein Quadrat, nicht ein Rechteck oder sonstiges Viereck? Ebenso für die blauen Vierecke, welche mit a^2, b^2, c^2 beschriftet sind. Außerdem im rechten Bild zu sehen: erste Binomische Formel.
    \end{figure}


    \subsection{Der überraschende Beweis -- Zusammensetzen}
    \vspace*{-1em}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=23em]{pythagoras/proof-pyth-steps.pdf}
        % \Animation: https://www.geogebra.org/m/jFFERBdd#material/S97y2bYE
        % Argumente: Wie wird das Quadrat C im rechten Bild genau konstruiert, d.h. wie finden wir den Punkt von C, der ganz unten ansetzt? -> Rechtwinkliges Dreieck, sodass der rechte Winkel auf unterer Seite liegt (zB mit Thales-Kreis), oder an unterer Seite die Seitenlängen a und b abtragen, Konstruktion ähnlich wie beim vorherigen Beweis (Schiebung). Und Warum ist C ein Quadrat?
    \end{figure}


    \subsection{Der lässige Beweis -- Scherung}
    \vspace*{-1em}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=41em]{pythagoras/proof-pyth-shear.pdf}
        % Animation: https://www.geogebra.org/m/jFFERBdd#material/HUbe242t
        % Argumente: Warum treffen sich die Ecken der beiden Parallelogramme im zweiten Bild oben genau, anstatt versetzte Höhen zu haben? Warum ist die Länge der außeren Seiten dann genau c, sodass die Paralellogramme runtergeschoben in Bild 3 genau ins Quadrat C passen? Und warum war die Seite, an der sich die Parallelogramme aus A und B trafen, genau gleich lang, sodass wir am Ende genau C ausfüllen nach der Scherung?
    \end{figure}


    \subsection{Der knobelige Beweis -- Partition}
    \vspace*{-1em}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=32em]{pythagoras/proof-pyth-bisection.pdf}
        % Animation: https://www.geogebra.org/m/jFFERBdd#material/ATE2XV8p
        % Argumente: Hier muss einfach bei jedem Stück nachgeprüft werden, ob Winkel und Seitenlängen zusammenpassen.
    \end{figure}


    \newpage

    \section{Bonus: Praktische Anwendungen des Satzes}

    Mithilfe des Satzes des Pythagoras konnten wir mit Leichtigkeit unser handwerkliches Problem in Aufgabe~\ref{ex:pyth} (auf Seite~\pageref{ex:pyth}) lösen. Tatsächlich gibt es auch viele andere Probleme, bei denen ein Teil der Lösung die Anwendung unseres Satzes erfordert. \\

    Der größte Bereich dieser Probleme wurde zu einem eigenen Gebiet der Mathematik, der \emph{Trigonometrie}. Grob gesagt, erlaubt uns die Trigonometrie, alle möglichen Strecken voneinander zu erlangen, wenn es um Dreiecke und Kreise geht. Wenn du schon ein bisschen Erfahrung hast mit der Zerlegung von Vielecken in Dreiecke, merkst du vielleicht, wie mächtig die Trigonometrie ist. Spätestens in der 10. Klasse wirst du mehr darüber erfahren. Die Hauptwerkzeuge der Trigonometrie sind die Sinus- und Cosinus-Funktion, geschrieben $\sin$ und $\cos$ -- halte Ausschau nach ihnen! \\

    Wir geben uns deswegen vorerst zufrieden mit drei einfacheren praktischen Anwendungen.

    \begin{aufgabe}
        Wie lang ist die Diagonale eines Rechtecks mit Seitenlängen $a$ und $b$?
    \end{aufgabe}

    \begin{aufgabe}
        In einem (zweidimensionalen) Koordinatensystem kann ein Punkt platziert werden, den wir dann eindeutig beschreiben können durch seine $x$- und $y$-Koordinate. Zum Beispiel wenn $x = 3$ und $y = 4$, schreiben wir oft $(3 | 4)$.

        Den Punkt $(0 | 0)$ nennen wir \emph{Ursprung}.

        \begin{enumerate}[a)]
            \item Wie lang ist die Strecke vom Punkt $(3 | 4)$ zum Ursprung $(0 | 0)$? % -> sqrt(9 + 16) = 5
            \item Wie lang ist die Strecke vom Punkt $(x | y)$ zum Ursprung $(0 | 0)$? % sqrt(x^2 + y^2)
            \item \textbf{Bonus:} Wie lang ist die Strecke vom Punkt $(-5 | 1)$ zum Punkt $(10 | 9)$? % -> sqrt(289) = 17
        \end{enumerate}
    \end{aufgabe}

    \enlargethispage{2\baselineskip}

    \begin{aufgabe}
        Ein Quader ist ein dreidimensionaler Körper, dessen Seitenflächen aus sechs Rechtecken besteht, wobei zwei gegenüberliegende Rechtecke genau gleich groß sind. Ein Würfel ist auch ein Quader, aber einer mit sechs genau gleich großen Rechtecken, die damit auch Quadrate sind.

        \begin{enumerate}[a)]
            \item Berechne mithilfe des Satzes von Pythagoras die Länge der Diagonale eines Würfels mit der Seitenlänge $4$. % sqrt(16 + 16 + 16) = sqrt(48) = fast 7
            \item Berechne mithilfe des Satzes von Pythagoras die Länge der Diagonale eines Quaders mit den Seitenlängen $3, 4, 12$. % sqrt(9 + 16 + 144) = sqrt(169) = 13
            \item Kannst du eine allgemeine Formel finden, mit der die Länge der Diagonale eines Quaders mit beliebigen Seitenlängen $a, b, c$ berechnet werden kann? % sqrt(a^2 + b^2 + c^2)
        \end{enumerate}
    \end{aufgabe}



% Coole Abbildungen von Beweisen: https://dewiki.de/Lexikon/Satz_des_Pythagoras

% Es gibt einen Beweis vom Satz des Pythagoras in Gedichtform, aus dem 19. Jahrhundert 🤯
% https://hjschlichting.wordpress.com/2017/03/09/satz-des-pythagoras-formen-der-ewigen-magie/
% EDIT: hier in der Originalsprache: https://kalliope.org/da/text/andersen2001091301
% und hier 60 mal ohne worte: https://www.geogebra.org/m/jFFERBdd
% https://www.cut-the-knot.org/pythagoras/ da sind sonst genug andere


\end{document}