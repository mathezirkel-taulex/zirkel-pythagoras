Zirkel (Pythagoras 1) wurde als zweite Hälfte des Kennenlernzirkels Präsenzzirkel Klasse 7/8 2022/23 konzipiert und gehalten. Von Alex Mai.

Das Problem auf der ersten Seite war gut greifbar, aber überraschenderweise konnte niemand es lösen -- was im Konzept auch nicht unbedingt vorgesehen ist. Ein Kind (von 4 anwesenden) ahnte schon, dass es mit dem Satz des Pythagoras zu tun hat, aber wusste nicht, wie man ihn anwendet.

Viel Zeit wurde dann damit verbracht, gemeinsam die Formeln für die Flächeninhalte der einfachen Formen (Quadrat, Rechteck, Parallelogramm, Dreieck) zu verstehen. Zwei der anwesenden Kinder meinten, das noch nie gesehen zu haben bzw zu wissen, dass es pandemiebedingt in den letzten beiden Jahren ausgelassen worden sei. Die anderen beiden Kinder waren super fit im Flächeninhalte berechnen und hatten auch Spaß dabei.

Den Satz und seine visuelle Interpretation dann auf Seite 3 haben wir uns nur kurz angeschaut am Ende. Einen ersten visuellen beweis habe ich nur geteasert, aber niemand konnte ihn auf die schnelle verstehen ohne weitere Bilder (war nicht erwartet), was die Kinder durchaus gespannt auf den zweiten Teil des Zirkels gelassen hat, in dem dann mit vollen 90 Minuten eine Mischung aus Anwendung und Beweise des Satzes anstehen.

Anregungen für die Zukunft:
- Bei den Formen auf Seite 2 auch in der Aufgabenstellung dazu anregen, den Umfang zu berechnen und sich die Formeln von Umfang und Fläche mit Skizzen zu veranschaulichen
